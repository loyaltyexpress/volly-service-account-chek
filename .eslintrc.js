module.exports = {
  extends: "@volly/eslint-config-volly/backend",
  rules: {
    // allow private class vars to start with _:
    "no-underscore-dangle": "off",

    // allow helper functions with a class to not have to implement this:
    "class-methods-use-this": "off",

    // allow for single functions within modules to be named exports:
    "import/prefer-default-export": "off",

    "no-useless-constructor": "off",

    // allow for ! operator to assert var is not null:
    "@typescript-eslint/no-non-null-assertion": "off",

    // allow_different_casing:
    "@typescript-eslint/camelcase": "off",

    "@typescript-eslint/explicit-module-boundary-types": "off",

    "no-param-reassign": "off",
  },
};
