module.exports = {
  bracketSpacing: true,
  endOfLine: "auto",
  printWidth: 100,
  semi: true,
  tabWidth: 2,
  trailingComma: "es5",
};
