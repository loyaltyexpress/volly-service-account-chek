FROM node:14-alpine as builder

# ---------- Build Service -----------#

WORKDIR /app

# Copy the npmrc file, which will allow us to download the private Volly libraries from npm
COPY .npmrc ./

# Install the dependencies for the service
COPY package*.json ./
RUN npm i

# Copy over only the stuff needed to build the app
COPY tsconfig.json ./
COPY src ./src

# Build the service
RUN npm run build

# ---------- Create Final Image -----------#

# Install/build service
FROM node:14-alpine as final

WORKDIR /app

# Copy the service files over
COPY --from=builder /app/dist ./dist
COPY .npmrc ./
COPY package*.json ./

# Install only the dependencies (not dev-dependencies)
RUN npm ci --only=production

# If you want to run locally, uncomment this to copy the .env file in to get config values.
#COPY .env ./

# Install Curl
RUN apk --no-cache add curl

# Install PM2
RUN npm install pm2@latest -g

# Start the service
CMD pm2-runtime start ./dist/server.js --name volly

# The service is running on port 8060 so expose it.  The port needs to be mapped when running the container
EXPOSE 8060