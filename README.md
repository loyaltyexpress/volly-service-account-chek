# volly-service-account-chek

## Installation

```
git clone https://bitbucket.org/loyaltyexpress/volly-service-account-chek.git

npm install

npm run debug
```

NOTE: service will automatically build up the table structure, but requires the existance of a volly database in whatever credentials are supplied in the .env file.

## Tests

You can run tests by executing the following command

```
npm run test
```

The tests will spin up a a new schema (account-chek_test) and will run tests against that schema. The schema will get removed after the tests run

## More Documentation
