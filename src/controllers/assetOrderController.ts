import { Logger } from "@volly/logging";
import Router from "@koa/router";
import Koa from "koa";
import { Inject, Service } from "typedi";
import BaseController from "./baseController";
import CreateAssetOrderHandler from "../handlers/createAssetOrderHandler";
import GetIframeUrlHandler from "../handlers/getIframeUrlHandler";
import GetLiteDataHandler from "../handlers/getLiteDataHandler";

@Service()
export default class AssetOrderController extends BaseController {
  // inject any needed handlers
  @Inject() private _createAssetOrderHandler: CreateAssetOrderHandler;

  @Inject() private _getIframeUrlHandler: GetIframeUrlHandler;

  @Inject() private _getLiteDataHandler: GetLiteDataHandler;

  public assetOrderRouter: Router;

  public initialize = (): any => {
    this.assetOrderRouter = new Router({ prefix: "/asset" });
    this.startOrder();
    this.getIframeUrl();
    this.getLiteData();
    return this.assetOrderRouter.routes();
  };

  public startOrder = (): any => {
    this._swaggerDefGenerator.registerOperation({
      path: "/asset",
      method: "post",
      summary: "Create an asset order",
      description: "Create an asset order",
      responses: [{ statusCode: "201", description: "OK" }],
      tags: ["Asset"],
    });

    this.assetOrderRouter.post("/", async (ctx: Koa.Context) => {
      const logger: Logger = ctx.log;

      let token = ctx.query.token ?? "";
      if (Array.isArray(token)) token = token[0];

      const response = await this._createAssetOrderHandler.start(logger, token, ctx.request.body);
      ctx.response.status = 201;
      ctx.response.body = response;
    });
  };

  public getIframeUrl = (): any => {
    this._swaggerDefGenerator.registerOperation({
      path: "/asset/{referenceId}/url",
      method: "get",
      summary: "Get asset iframe url",
      description: "Get asset iframe url",
      responses: [{ statusCode: "200", description: "OK" }],
      tags: ["Asset"],
    });

    this.assetOrderRouter.get("/:referenceId/url", async (ctx: Koa.Context) => {
      const logger: Logger = ctx.log;
      const { referenceId } = ctx.params;

      let token = ctx.query.token ?? "";
      if (Array.isArray(token)) token = token[0];

      const response = await this._getIframeUrlHandler.getUrl(logger, token, referenceId);
      ctx.response.status = 200;
      ctx.response.body = response;
    });
  };

  public getLiteData = (): any => {
    this._swaggerDefGenerator.registerOperation({
      path: "/asset/{referenceId}/data",
      method: "get",
      summary: "Get order asset data",
      description: "Get order asset data",
      responses: [{ statusCode: "200", description: "OK" }],
      tags: ["Asset"],
    });

    this.assetOrderRouter.get("/:referenceId/data", async (ctx: Koa.Context) => {
      const logger: Logger = ctx.log;
      const { referenceId } = ctx.params;

      let token = ctx.query.token ?? "";
      if (Array.isArray(token)) token = token[0];

      const response = await this._getLiteDataHandler.getLiteData(logger, token, referenceId);
      ctx.response.status = 200;
      ctx.response.body = response;
    });
  };
}
