import { ConfigResponse } from "@volly/config";
import { SwaggerDefinitionGenerator } from "@volly/utils";
import { Inject, Service } from "typedi";

// baseController contains common services required in all child controllers
@Service()
export default class BaseController {
  @Inject("service-config") protected _serviceConfig: ConfigResponse;

  @Inject("swagger-def-generator") protected _swaggerDefGenerator: SwaggerDefinitionGenerator;
}
