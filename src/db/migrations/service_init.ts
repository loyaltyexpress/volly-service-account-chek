import { Knex } from "knex";
import { addTimestamps } from "../utils/util";

const schema = process.env.PG_DB_SCHEMA || "public";

export async function up(knex: Knex): Promise<void> {
  // create asset_orders table
  await knex.schema
    .withSchema(schema)
    .createTable("asset_orders", (table) => {
      table.increments("asset_order_id").primary().notNullable();

      table.string("company_id", 36).notNullable();

      table.string("reference_id", 36).notNullable();

      table.string("order_id", 36).nullable();

      table.string("tos_timestamp", 36).nullable();

      table.string("report_id", 36).nullable();

      table.boolean("report_available").notNullable();
    })
    .then(() => addTimestamps(knex, "asset_orders", schema));

  // create transactions table
  await knex.schema
    .withSchema(schema)
    .createTable("asset_transactions", (table) => {
      table.increments("asset_transaction_id").primary().notNullable();

      table
        .integer("asset_order_id")
        .references("asset_order_id")
        .inTable(`${schema}.asset_orders`)
        .notNullable();

      table.string("transaction_type", 20).notNullable();

      table.jsonb("data_in").nullable();

      table.jsonb("data_out").nullable();

      table.jsonb("data_response").nullable();

      table.integer("status").nullable();

      table.string("message", 1000).nullable();
    })
    .then(() => addTimestamps(knex, "asset_transactions", schema));
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.withSchema(schema).dropTableIfExists("asset_orders");
  await knex.schema.withSchema(schema).dropTableIfExists("transactions");
}
