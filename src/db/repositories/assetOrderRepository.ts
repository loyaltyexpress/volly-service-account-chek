import _ from "lodash";
import { Knex } from "knex";
import { Service, Inject } from "typedi";
import { Logger } from "@volly/logging";
import Repository from "./repository";
import AssetOrder from "../../models/internal/assetOrder";

@Service({ global: true })
export default class AssetOrderRepository extends Repository<AssetOrder> {
  constructor(@Inject("db_read") dbRead: Knex, @Inject("db_write") dbWrite: Knex) {
    super(dbRead, dbWrite, "assetOrders", "assetOrderId", (data: any): AssetOrder => {
      if (data && !_.isEmpty(data)) {
        if (Array.isArray(data)) return new AssetOrder(data[0]);
        return new AssetOrder(data);
      }
      return new AssetOrder();
    });

    this._schema = process.env.PG_DB_SCHEMA || "public";
  }

  getByReferenceId(referenceId: string, logger: Logger, metadata: any): Promise<AssetOrder | null> {
    return this._dbRead(this._table)
      .withSchema(this._schema)
      .where("referenceId", referenceId)
      .then((data: any) => {
        if (!data) return null;
        if (Array.isArray(data)) {
          if (data.length > 0) return this._objCreator(data[0]);
          return null;
        }
        return this._objCreator(data);
      })
      .catch((err) => {
        if (logger) logger.error(err.message, err, metadata);
        throw new Error("Database error");
      });
  }
}
