import _ from "lodash";
import { Knex } from "knex";
import { Service, Inject } from "typedi";
import Repository from "./repository";
import AssetTransaction from "../../models/internal/assetTransaction";

@Service({ global: true })
export default class AssetTransactionRepository extends Repository<AssetTransaction> {
  constructor(@Inject("db_read") dbRead: Knex, @Inject("db_write") dbWrite: Knex) {
    super(
      dbRead,
      dbWrite,
      "assetTransactions",
      "assetTransactionId",
      (data: any): AssetTransaction => {
        if (data && !_.isEmpty(data)) {
          if (Array.isArray(data)) return new AssetTransaction(data[0]);
          return new AssetTransaction(data);
        }
        return new AssetTransaction();
      }
    );

    this._schema = process.env.PG_DB_SCHEMA || "public";
  }
}
