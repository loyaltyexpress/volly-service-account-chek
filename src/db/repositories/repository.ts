import { Knex } from "knex";
import { Logger } from "@volly/logging";

export default abstract class Repository<T> {
  protected _dbWrite: Knex;

  protected _dbRead: Knex;

  protected _table: string;

  protected _primaryKey: string;

  protected _schema: string;

  protected _objCreator: (data: any) => T | null;

  constructor(
    dbRead: Knex,
    dbWrite: Knex,
    table: string,
    primaryKey: string,
    objCreator: (data: T) => T | null
  ) {
    this._dbRead = dbRead;
    this._dbWrite = dbWrite;
    this._table = table;
    this._primaryKey = primaryKey;
    this._objCreator = objCreator;
  }

  public getById = (id: string | number, logger?: Logger, metadata?: any): Promise<T | null> => {
    return this._dbRead(this._table)
      .withSchema(this._schema)
      .where(this._primaryKey, id)
      .then((data: any) => {
        if (!data) return null;
        return this._objCreator(data);
      })
      .catch((err) => {
        if (logger) logger.error(err.message, err, metadata);
        throw new Error("Database error");
      });
  };

  public insert = (model: T, logger?: Logger, metadata?: any): Promise<any> => {
    return this._dbWrite(this._table)
      .withSchema(this._schema)
      .insert(model, this._primaryKey)
      .then((data: any) => {
        return data.length > 0 ? data[0] : 0;
      })
      .catch((err) => {
        if (logger) logger.error(err.message, err, metadata);
        throw new Error("Database error");
      });
  };

  public update = (
    id: string | number,
    model: T,
    logger?: Logger,
    metadata?: any
  ): Promise<any> => {
    return this._dbWrite(this._table)
      .withSchema(this._schema)
      .where(this._primaryKey, id)
      .update(model)
      .returning(this._primaryKey)
      .catch((err) => {
        if (logger) logger.error(err.message, err, metadata);
        throw new Error("Database error");
      });
  };

  public delete = (id: string | number): Promise<any> => {
    return this._dbWrite(this._table).withSchema(this._schema).where(this._primaryKey, id).delete();
  };
}
