import { Knex } from "knex";

export function addTimestamps(knex: Knex, tableName: string, schema: string) {
  return knex.schema
    .withSchema(schema)
    .alterTable(tableName, (t: any) => {
      t.timestamp("created_at").defaultTo(knex.fn.now());
      t.timestamp("modified_at").defaultTo(knex.fn.now());
    })
    .then(() => {
      // ensures the function exists, then add the table trigger
      return knex.raw(`
        CREATE OR REPLACE FUNCTION ${schema}.update_modified_column()
        RETURNS TRIGGER AS $$
        BEGIN
          NEW.modified_at = now();
          RETURN NEW;
        END;
        $$ language 'plpgsql';

        CREATE TRIGGER update_${tableName}_modified_at
        BEFORE UPDATE ON ${schema}.${tableName}
        FOR EACH ROW
        EXECUTE PROCEDURE ${schema}.update_modified_column();
      `);
    });
}
