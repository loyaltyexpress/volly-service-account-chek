import { ConfigResponse } from "@volly/config";
import { Inject, Service } from "typedi";
import AccountChekApiClient from "../util/accountChekApiClient";

// baseHandler contains common services required in all child handlers
@Service()
export default class BaseHandler {
  @Inject("service-config")
  protected _serviceConfig: ConfigResponse;

  @Inject("account-chek-api-client")
  protected _accountChekApiClient: AccountChekApiClient;
}
