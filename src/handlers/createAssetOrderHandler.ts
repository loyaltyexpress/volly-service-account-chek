import { Logger } from "@volly/logging";
import { Inject, Service } from "typedi";
import { ApiError } from "@volly/utils";
import BaseHandler from "./baseHandler";
import AssetOrderRepository from "../db/repositories/assetOrderRepository";
import TransactionRepository from "../db/repositories/assetTransactionRepository";
import AssetOrderRequest from "../models/internal/assetOrderRequest";
import AssetTransaction from "../models/internal/assetTransaction";
import AssetOrder from "../models/internal/assetOrder";
import AccountChekOrder from "../models/accountChek/accountChekOrder";
import AccountChekOrderResponse from "../models/accountChek/accountChekOrderResponse";

@Service()
export default class CreateAssetOrderHandler extends BaseHandler {
  @Inject()
  private _assetOrdersRepository: AssetOrderRepository;

  @Inject()
  private _transactionRepository: TransactionRepository;

  public start = async (
    logger: Logger,
    token: string,
    request: AssetOrderRequest
  ): Promise<any> => {
    const metadata: any = {
      referenceId: request.referenceId,
      companyId: request.companyId,
    };

    if (token.length === 0) throw new ApiError("Token not found", 403, "", metadata);

    logger.debug("Checking for the existance of an order with that same referenceId", metadata);
    let assetOrder: AssetOrder | null = await this._assetOrdersRepository.getByReferenceId(
      request.referenceId,
      logger,
      metadata
    );

    if (assetOrder) {
      // TODO - if the asset order already exists, just return the id
    }

    assetOrder = new AssetOrder({
      companyId: request.companyId,
      referenceId: request.referenceId,
      reportAvailable: false,
    });

    if (request.tosTimestamp) assetOrder.tosTimestamp = new Date(request.tosTimestamp);

    // create an order immediately
    assetOrder.assetOrderId = await this._assetOrdersRepository.insert(assetOrder);

    // create a transaction
    const obfuscatedOrderRequest = new AssetOrderRequest({ ...request });
    obfuscatedOrderRequest.obfuscate();

    const transaction: AssetTransaction = new AssetTransaction({
      assetOrderId: assetOrder.assetOrderId!,
      dataIn: obfuscatedOrderRequest,
      transactionType: "START_ORDER",
    });
    transaction.assetTransactionId = await this._transactionRepository.insert(
      transaction.convertToDbTransaction(),
      logger,
      metadata
    );

    const response = {
      id: "",
    };

    try {
      const baseUrl = this._serviceConfig.data.api_url;

      // create the asset order with the account chek api
      const accountCheckOrder: AccountChekOrder = new AccountChekOrder(request);
      const createOrderResponse: AccountChekOrderResponse =
        await this._accountChekApiClient.createOrder(
          token,
          baseUrl,
          accountCheckOrder,
          logger,
          metadata
        );

      // hide important data and store the json that was sent to the account chek api
      accountCheckOrder.obfuscate();
      createOrderResponse.obfuscate();
      transaction.dataOut = accountCheckOrder;
      transaction.dataResponse = createOrderResponse;
      assetOrder.orderId = createOrderResponse.id;
      response.id = assetOrder.orderId;

      // attach VOA service to order
      const attachResponse: any = await this._accountChekApiClient.attachServiceToOrder(
        token,
        baseUrl,
        assetOrder.orderId,
        logger,
        metadata
      );
      if (attachResponse.status !== 201) {
        logger.error(attachResponse.statusText);
        throw new ApiError("Attaching VOA service was not successful.", 400);
      }

      const setStatusResponse: any = await this._accountChekApiClient.setOrderStatus(
        token,
        baseUrl,
        assetOrder.orderId,
        logger,
        metadata
      );
      if (setStatusResponse.status !== 200) {
        logger.error(attachResponse.statusText);
        throw new ApiError("Setting status was not successful.", 400);
      }

      transaction.dataResponse.status = setStatusResponse.data.status;
      transaction.status = 200;
    } catch (e) {
      transaction.status = e.status ? e.status : 500;
      transaction.message = e.message;
      logger.error(e.message, e);
    } finally {
      await this._transactionRepository.update(transaction.assetTransactionId!, transaction);
      await this._assetOrdersRepository.update(assetOrder.assetOrderId, assetOrder);
    }

    return response;
  };
}
