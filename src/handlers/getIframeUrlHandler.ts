import { Logger } from "@volly/logging";
import { Inject, Service } from "typedi";
import { ApiError } from "@volly/utils";
import BaseHandler from "./baseHandler";
import AssetOrderRepository from "../db/repositories/assetOrderRepository";
import TransactionRepository from "../db/repositories/assetTransactionRepository";
import AssetTransaction from "../models/internal/assetTransaction";
import AssetOrder from "../models/internal/assetOrder";
import AccountChekIframeUrlResponse from "../models/accountChek/accountChekIframeUrlResponse";

@Service()
export default class GetIframeUrlHandler extends BaseHandler {
  @Inject()
  private _assetOrdersRepository: AssetOrderRepository;

  @Inject()
  private _transactionRepository: TransactionRepository;

  public getUrl = async (
    logger: Logger,
    token: string,
    referenceId: string
  ): Promise<AccountChekIframeUrlResponse | null> => {
    const metadata: any = {
      referenceId,
    };

    if (token.length === 0) throw new ApiError("Token not found", 403, "", metadata);

    logger.debug("Checking for the existance of an order with that same referenceId", metadata);
    const assetOrder: AssetOrder | null = await this._assetOrdersRepository.getByReferenceId(
      referenceId,
      logger,
      metadata
    );

    if (!assetOrder) {
      throw new ApiError("No order found with that reference id", 404);
    }
    if (!assetOrder.orderId) {
      throw new ApiError("Order found, but is missing the orderId", 400);
    }

    const transaction: AssetTransaction = new AssetTransaction({
      assetOrderId: assetOrder.assetOrderId!,
      transactionType: "GET_IFRAME_URL",
    });
    transaction.assetTransactionId = await this._transactionRepository.insert(
      transaction.convertToDbTransaction(),
      logger,
      metadata
    );

    let response: AccountChekIframeUrlResponse | null = null;

    try {
      const baseUrl = this._serviceConfig.data.api_url;

      // get the iframe url
      response = await this._accountChekApiClient.getIframeUrl(
        token,
        baseUrl,
        assetOrder.orderId,
        logger,
        metadata
      );

      transaction.dataResponse = response;
      transaction.status = 200;
    } catch (e) {
      transaction.status = e.status ? e.status : 500;
      transaction.message = e.message;
      logger.error(e.message, e);
    } finally {
      await this._transactionRepository.update(transaction.assetTransactionId!, transaction);
    }

    return response;
  };
}
