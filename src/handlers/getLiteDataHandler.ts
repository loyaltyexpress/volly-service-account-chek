import { Logger } from "@volly/logging";
import { Inject, Service } from "typedi";
import { ApiError } from "@volly/utils";
import { PosAsset } from "@volly/models";
import BaseHandler from "./baseHandler";
import AssetOrderRepository from "../db/repositories/assetOrderRepository";
import TransactionRepository from "../db/repositories/assetTransactionRepository";
import AssetTransaction from "../models/internal/assetTransaction";
import AssetOrder from "../models/internal/assetOrder";
import AccountChekLiteDataResponse from "../models/accountChek/accountChekLiteDataResponse";

type PosAccountType =
  | "checking"
  | "savings"
  | "moneymarket"
  | "cd"
  | "mutualfund"
  | "retirement"
  | "stock"
  | "stockoptions"
  | "bonds"
  | "bridgeloanproceeds"
  | "individualdevelopmentaccount"
  | "trust"
  | "cashvaluelifeinsurance"
  | "earnestmoney"
  | "proceedsnonrealestate"
  | "proceedsrealestate"
  | "sweatequity"
  | "employerassistance"
  | "gift"
  | "rentcredit"
  | "securedborrowerfunds"
  | "tradeequity"
  | "unsecuredborrowerfunds"
  | "cashdeposit"
  | "other"
  | "sellercredit"
  | "lendercredit"
  | "leasepurchasefund"
  | "otherpurchasecredit";

@Service()
export default class GetLiteDataHandler extends BaseHandler {
  @Inject()
  private _assetOrdersRepository: AssetOrderRepository;

  @Inject()
  private _transactionRepository: TransactionRepository;

  public getLiteData = async (
    logger: Logger,
    token: string,
    referenceId: string
  ): Promise<Array<PosAsset>> => {
    const metadata: any = {
      referenceId,
    };

    if (token.length === 0) throw new ApiError("Token not found", 403, "", metadata);

    logger.debug("Checking for the existance of an order with that same referenceId", metadata);
    const assetOrder: AssetOrder | null = await this._assetOrdersRepository.getByReferenceId(
      referenceId,
      logger,
      metadata
    );

    if (!assetOrder) {
      throw new ApiError("No order found with that reference id", 404);
    }
    if (!assetOrder.orderId) {
      throw new ApiError("Order found, but is missing the orderId", 400);
    }

    const transaction: AssetTransaction = new AssetTransaction({
      assetOrderId: assetOrder.assetOrderId!,
      transactionType: "GET_LITE_DATA",
    });
    transaction.assetTransactionId = await this._transactionRepository.insert(
      transaction.convertToDbTransaction(),
      logger,
      metadata
    );

    const assets: Array<PosAsset> = [];

    try {
      const baseUrl = this._serviceConfig.data.api_url;

      // get the lite data
      const getLiteDataResponse: any = await this._accountChekApiClient.getLiteData(
        token,
        baseUrl,
        assetOrder.orderId,
        logger,
        metadata
      );

      const logResponse: Array<AccountChekLiteDataResponse> = [];
      if (getLiteDataResponse && getLiteDataResponse.length && getLiteDataResponse.length > 0) {
        getLiteDataResponse.forEach((account: any) => {
          const posAsset: PosAsset = {
            institution_name: `${account.fiName} - ${account.accountName}`,
            asset_account_type: this.mapAccountType(account.fiAccountType),
            asset_entry_method: "accountchek",
            account_number: account.accountNumber,
            balance: account.balance,
            asset_type: "account",
            market_value: account.balance,
            deposited: account.balance,
          };

          assets.push(posAsset);

          const logAccount = new AccountChekLiteDataResponse(account);
          logAccount.obfuscate();
          logResponse.push(logAccount);
        });
      }

      transaction.dataResponse = JSON.stringify(logResponse);
      transaction.status = 200;
    } catch (e) {
      transaction.status = e.status ? e.status : 500;
      transaction.message = e.message;
      logger.error(e.message, e);
    } finally {
      await this._transactionRepository.update(transaction.assetTransactionId!, transaction);
    }

    return assets;
  };

  private mapAccountType(leadCloudAccountType: string): PosAccountType {
    switch (leadCloudAccountType) {
      case "checking":
      case "Checking":
        return "checking";
      case "savings":
      case "Savings":
        return "savings";
      case "cd":
      case "CD":
      case "certificateofdeposit":
      case "CertificateOfDeposit":
        return "cd";
      case "moneymarket":
      case "money market":
      case "MoneyMarket":
      case "Money Market":
        return "moneymarket";
      default:
        return "checking";
    }
  }
}
