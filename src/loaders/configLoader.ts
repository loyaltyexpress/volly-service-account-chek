import ConfigSDK, { ConfigResponse, IConfigParameter } from "@volly/config";
import { SwaggerDefinitionGenerator } from "@volly/utils";
import { Logger } from "@volly/logging";
import { externalTaskRunner } from "@volly/los-services";
import { platform } from "os";
import Container from "typedi";
import AccountChekApiClient from "../util/accountChekApiClient";

type ConfigLoaderParams = {
  logger: Logger;
  serviceName: string;
  serviceConfig: IConfigParameter[];
};

export default class ConfigLoader {
  private _serviceConfig: IConfigParameter[];

  private _logger: Logger;

  private _serviceName: string;

  private _env: string;

  private _osType: string;

  constructor(params: ConfigLoaderParams) {
    this._logger = params.logger;
    this._serviceConfig = params.serviceConfig;
    this._serviceName = params.serviceName;
    this._env = process.env.NODE_ENV ?? "cit";
    this._osType = platform();
  }

  private handleConfigChange = async () => {
    if (this._env !== "development")
      // the 2nd parameter of the array, "volly" in this case, needs to match the dockerfile when pm2 starts using the --name flag
      await externalTaskRunner("pm2", ["reload", "volly"], this._logger, this._osType === "win32");
  };

  public init = async (): Promise<ConfigResponse> => {
    // Create the swagger defition object that will store all swagger definitions.
    const swaggerDefGenerator = new SwaggerDefinitionGenerator();
    Container.set("swagger-def-generator", swaggerDefGenerator);

    const configSDK: ConfigSDK = new ConfigSDK({
      notifyCallback:
        this._env !== "development" && this._env !== "test"
          ? this.handleConfigChange.bind(this)
          : null,
    });

    // NOTE:  do not change this config object.  Doing so will cause the service to keep restarting.  If you need to alter a value, clone the config object
    // into a new one and use that.
    const config = await configSDK.get(this._serviceConfig);

    // Set environment variables that the knexfile/knexLoader will use.
    process.env.PG_DB = config.data.postgres_credentials.dbname;
    process.env.PG_DB_HOST_WRITE = config.data.postgres_credentials.host_write;
    process.env.PG_DB_HOST_READ = config.data.postgres_credentials.host_read;
    process.env.PG_DB_PORT = config.data.postgres_credentials.port.toString();
    process.env.PG_DB_USER = config.data.postgres_credentials.username;
    process.env.PG_DB_PASSWORD = config.data.postgres_credentials.password;
    process.env.PG_DB_SCHEMA = config.data.postgres_credentials.schema;

    // add the application config to a DI container so consumers can inject
    Container.set("service-config", config);
    Container.set("account-chek-api-client", new AccountChekApiClient());

    this._logger.info("[SERVER] Config initialized.");
    return config;
  };
}
