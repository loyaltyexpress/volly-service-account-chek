import { knex, Knex } from "knex";
import { Container } from "typedi";
import { Logger } from "@volly/logging";
import { postProcessResponse, wrapIdentifier } from "@volly/los-services";

const knexLoader = (logger: Logger): Knex => {
  const configWrite: any = {
    client: "pg",
    connection: {
      host: process.env.PG_DB_HOST_WRITE,
      user: process.env.PG_DB_USER,
      password: process.env.PG_DB_PASSWORD,
      database: process.env.PG_DB,
      port: process.env.PG_DB_PORT,
      schema: process.env.PG_DB_SCHEMA,
      ssl: {
        rejectUnauthorized: false,
      },
    },
    wrapIdentifier,
    postProcessResponse,
  };

  // if development, don't use ssl
  if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test")
    configWrite.connection.ssl = false;

  const dbWrite = knex(configWrite);
  Container.set({
    id: "db_write",
    factory: () => dbWrite,
    global: true,
  });

  const configRead = { ...configWrite };
  configRead.connection.host = process.env.PG_DB_HOST_READ;

  const dbRead = knex(configRead);
  Container.set({
    id: "db_read",
    factory: () => dbRead,
    global: true,
  });

  logger.info("database initialized");
  return dbWrite;
};

export default knexLoader;
