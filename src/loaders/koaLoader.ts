import { ConfigResponse } from "@volly/config";
import { Logger } from "@volly/logging";
import { KoaConfig, KoaFactory } from "@volly/utils";
import Koa from "koa";
import { Container } from "typedi";

import AssetOrderController from "../controllers/assetOrderController";

export default class KoaLoader {
  private _logger: Logger;

  private _serviceConfig: ConfigResponse;

  constructor(logger: Logger) {
    this._logger = logger;
    this._serviceConfig = Container.get("service-config");
  }

  public init = async (): Promise<Koa> => {
    // note - don't parse using zod. It doesn't handle the swagger.generator properly
    const koaConfig: KoaConfig = {
      serviceId: this._serviceConfig.data.service_id,
      serviceUrl: this._serviceConfig.data.service_url,
      swagger: {
        enabled: true,
        routePrefix: "swagger",
        definitionUrl: "swaggerDefinition",
        specPrefix: process.env.NODE_ENV === "development" ? "/" : "/api",
        generator: Container.get("swagger-def-generator"),
      },
      logging: {
        logLevel: this._serviceConfig.data.log_level,
        autoLogRequests: false,
        usePrettyPrint: this._serviceConfig.data.use_pretty_print,
      },
      // optional - use if you need auth support via the auth service endpoint
      oauth: {
        apiUrl: this._serviceConfig.data.private_url,
        ignoreRoutes: ["/healthcheck", "/swagger", "/swaggerDefinition", "/favicon.png"],
        // ignoreRoutes: ["*"],
        authRoutes: ["/auth/"],
        apiCode: "ack",
      },
    };

    // KoaFactory gives you a bunch of stuff out of the box including: security headers, bodyparser, healthcheck/swagger urls, auth handling
    const koa: Koa = KoaFactory.build(koaConfig);

    // controllers
    koa.use(Container.get(AssetOrderController).initialize());

    this._logger.info("[SERVER] koa initialized.");

    return koa;
  };
}
