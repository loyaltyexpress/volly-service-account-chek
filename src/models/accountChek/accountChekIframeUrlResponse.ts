export default class AccountChekIframeUrlResponse {
  constructor(obj?: Partial<AccountChekIframeUrlResponse>) {
    Object.assign(this, obj);
  }

  url: string;
}
