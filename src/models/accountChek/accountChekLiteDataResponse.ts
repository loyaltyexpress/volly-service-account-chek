export default class AccountChekLiteDataResponse {
  constructor(obj?: Partial<AccountChekLiteDataResponse>) {
    Object.assign(this, obj);
  }

  accountId: string;

  accountType: string;

  accountName: string;

  accountNumber: string;

  accountHolder: string;

  fiName: string;

  fiAccountType: string;

  fiPlanName: string;

  balance: number;

  balanceDate: string;

  public obfuscate() {
    this.accountName = "xxxxxx";
    this.fiName = "xxxxxx";
  }
}
