import AssetOrderRequest from "../internal/assetOrderRequest";
import AccountChekOrderAddress from "./accountChekOrderAddress";

export default class AccountChekOrder {
  constructor(request?: AssetOrderRequest) {
    if (request) {
      this.email = request.email;
      this.last4SSN = request.ssn.substring(request.ssn.length - 4, request.ssn.length);
      this.referenceNumber = request.referenceId;
      this.firstName = request.firstName;
      this.lastName = request.lastName;
      this.returnUrl = null; // TODO
      this.phoneNumber = null;
      this.employerName = request.employerName;
      this.ssn = request.ssn;
      this.dateOfBirth = request.dateOfBirth;
      this.address = new AccountChekOrderAddress(request);
    }
  }

  email: string | null;

  last4SSN: string;

  referenceNumber: string;

  firstName: string;

  lastName: string;

  returnUrl: string | null;

  phoneNumber: string | null;

  employerName: string;

  ssn: string;

  dateOfBirth: string;

  address: AccountChekOrderAddress;

  public obfuscate() {
    this.last4SSN = `*${this.ssn.substring(this.ssn.length - 3, this.ssn.length)}`;
    this.ssn = `***-**-*${this.ssn.substring(this.ssn.length - 3, this.ssn.length)}`;
  }
}
