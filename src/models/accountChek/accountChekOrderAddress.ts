import AssetOrderRequest from "../internal/assetOrderRequest";

export default class AccountChekOrderAddress {
  constructor(request?: AssetOrderRequest) {
    if (request) {
      this.street = request.street;
      this.street2 = request.street2;
      this.city = request.city;
      this.state = request.state;
      this.zip = request.zip;
    }
  }

  street: string;

  street2: string | null;

  city: string;

  state: string;

  zip: string;
}
