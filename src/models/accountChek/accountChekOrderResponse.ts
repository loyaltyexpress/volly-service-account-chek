import AccountChekOrder from "./accountChekOrder";

export default class AccountChekOrderResponse extends AccountChekOrder {
  id: string;

  created: string;

  isArchived: boolean;

  status: string;

  loginUrl: string;

  daysRemaining: number;

  employmentVerificationRequested: boolean;

  public load(obj: any) {
    Object.assign(this, obj);
  }
}
