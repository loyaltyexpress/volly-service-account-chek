export default class AssetOrder {
  constructor(obj?: Partial<AssetOrder>) {
    Object.assign(this, obj);
  }

  assetOrderId: number;

  companyId: string;

  // the pos loan id or something unique from whoever is using the service
  referenceId: string;

  orderId: string;

  tosTimestamp: Date | null;

  reportId: string | null;

  reportAvailable: boolean | null;

  createdAt: Date | null;

  modifiedAt: Date | null;
}
