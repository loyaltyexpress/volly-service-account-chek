export default class AssetOrderRequest {
  constructor(obj?: Partial<AssetOrderRequest>) {
    Object.assign(this, obj);
  }

  companyId: string;

  referenceId: string;

  tosTimestamp: string;

  firstName: string;

  lastName: string;

  email: string;

  employerName: string;

  ssn: string;

  dateOfBirth: string;

  street: string;

  street2: string | null;

  city: string;

  state: string;

  zip: string;

  public obfuscate() {
    this.ssn = `***-**-*${this.ssn.substring(this.ssn.length - 3, this.ssn.length)}`;
  }
}
