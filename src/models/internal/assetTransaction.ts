export default class AssetTransaction {
  constructor(obj?: Partial<AssetTransaction>) {
    Object.assign(this, obj);
  }

  public convertToDbTransaction(): any {
    const newObj = {
      ...this,
    };

    if (newObj.dataIn) {
      const dataIn = JSON.stringify(newObj.dataIn);
      const dataInEscaped = dataIn.replace("'", "''");
      newObj.dataIn = dataInEscaped;
    }

    if (newObj.dataOut) {
      const dataOut = JSON.stringify(newObj.dataOut);
      const dataOutEscaped = dataOut.replace("'", "''");
      newObj.dataOut = dataOutEscaped;
    }

    if (newObj.dataResponse) {
      const dataResponse = JSON.stringify(newObj.dataResponse);
      const dataResponseEscaped = dataResponse.replace("'", "''");
      newObj.dataResponse = dataResponseEscaped;
    }

    return newObj;
  }

  assetTransactionId: number | null | undefined;

  assetOrderId: number;

  transactionType: string;

  dataIn: any | null;

  dataOut: any | null;

  dataResponse: any | null;

  status: number | null;

  message: string | null;

  createdAt: Date | null;

  modifiedAt: Date | null;
}
