import "reflect-metadata";
import { Knex } from "knex";
import { Container } from "typedi";
import { Logger } from "@volly/logging";
import { IConfigParameter } from "@volly/config";
import { migrationRunner } from "@volly/los-services";
import dotenv from "dotenv";
import http from "http";
import ConfigLoader from "./loaders/configLoader";
import KoaLoader from "./loaders/koaLoader";
import knexLoader from "./loaders/knexLoader";

dotenv.config();

// these are the application secrets. add values to your .env file first,
// then add the variable name to this array. make sure these values exist
// in the AWS parameter & secret store before pushing to higher environments
const SERVICE_CONFIG: IConfigParameter[] = [
  { name: "service_id", type: "static" },
  { name: "service_port", type: "static" },
  { name: "service_url", type: "static" },
  { name: "use_pretty_print", type: "static" },
  { name: "log_level", type: "parameter", appCode: "ack" },
  { name: "private_url", type: "parameter", appCode: "auz" },
  { name: "api_url", type: "parameter", appCode: "ack" },
  { name: "postgres_credentials", type: "secret", appCode: "ack" },
];

// set SERVICE_ID for initial service init. value is later stored in config.data.service_id after config init
const SERVICE_ID = "volly-service-account-chek";

const bootstrap = async () => {
  // create a base logger.  Most requests will use the middleware logger.  This is just for non-request stuff
  const logger = new Logger({
    autoLogRequests: false,
    logLevel: "debug",
    serviceId: SERVICE_ID,
    usePrettyPrint: false,
  });

  // init config
  const config = await new ConfigLoader({
    logger,
    serviceName: SERVICE_ID,
    serviceConfig: SERVICE_CONFIG,
  }).init();

  // initialize knex (database)
  knexLoader(logger);

  logger.info("Running database migrations");
  await migrationRunner(config, logger, __dirname);

  // init koa
  const koa = await new KoaLoader(logger).init();

  // start server
  const server = http.createServer(koa.callback());
  server.listen(config.data.service_port, () => {
    logger.info(
      `[SERVER] ${config.data.service_id} listening on port ${config.data.service_port}.`
    );
  });

  // PM2 will trigger this on shutdown
  process.on("SIGINT", () => {
    logger.info("SIGINT signal received.");

    // Stops the server from accepting new connections and finishes existing connections.
    server.close((err) => {
      if (err) {
        logger.error(err.message, err);
        process.exit(1);
      }

      // close knex on shutdown
      const knexInstanceWrite: Knex<any, unknown[]> = Container.get("db_write");
      knexInstanceWrite.destroy();

      const knexInstanceRead: Knex<any, unknown[]> = Container.get("db_read");
      knexInstanceRead.destroy();
    });
  });
};

bootstrap();
