import { Service } from "typedi";
import { Logger } from "@volly/logging";
import axios from "axios";
import { ApiError } from "@volly/utils";
import AccountChekOrder from "../models/accountChek/accountChekOrder";
import AccountChekOrderResponse from "../models/accountChek/accountChekOrderResponse";
import AccountChekIframeUrlResponse from "../models/accountChek/accountChekIframeUrlResponse";

export interface IAccountChekApiClient {
  createOrder(
    token: string,
    baseUrl: string,
    request: AccountChekOrder,
    logger: Logger,
    metadata: any
  ): Promise<AccountChekOrderResponse>;
}

@Service("account-chek-api-client")
export default class AccountChekApiClient implements IAccountChekApiClient {
  createOrder(
    token: string,
    baseUrl: string,
    request: AccountChekOrder,
    logger: Logger,
    metadata: any
  ): Promise<AccountChekOrderResponse> {
    const url = `${baseUrl}/v1/accountchekorders`;

    const createOrderMetadata = {
      ...metadata,
      url,
    };

    if (logger) logger.info("Creating order in accountChek", createOrderMetadata);

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Basic ${token}`,
      },
    };

    return axios.post(url, request, config).then((response: any) => {
      const accountChekOrderResponse = new AccountChekOrderResponse();
      accountChekOrderResponse.load(response.data);
      return accountChekOrderResponse;
    });
  }

  attachServiceToOrder(
    token: string,
    baseUrl: string,
    orderId: string,
    logger: Logger,
    metadata: any
  ): Promise<any> {
    const url = `${baseUrl}/v1/accountchekorders/${orderId}/voa`;

    const attachServiceMetadata = {
      ...metadata,
      url,
    };

    if (logger) logger.info("Attaching service to order in accountChek", attachServiceMetadata);

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Basic ${token}`,
      },
    };

    const payload = {
      requestType: "lite", // or full
      accountMonitoring: 90, // how long the order will stay open to re-verify assets
      daysBack: 90, // how many days of transaction history
    };

    return axios.post(url, payload, config);
  }

  setOrderStatus(
    token: string,
    baseUrl: string,
    orderId: string,
    logger: Logger,
    metadata: any
  ): Promise<any> {
    const url = `${baseUrl}/v1/accountchekorders/${orderId}`;

    const setOrderStatusMetadata = {
      ...metadata,
      url,
    };

    if (logger) logger.info("Setting status of order to opened", setOrderStatusMetadata);

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Basic ${token}`,
      },
    };

    const payload = {
      status: "opened",
    };

    return axios.patch(url, payload, config);
  }

  getIframeUrl(
    token: string,
    baseUrl: string,
    orderId: string,
    logger: Logger,
    metadata: any
  ): Promise<AccountChekIframeUrlResponse> {
    const url = `${baseUrl}/v1/accountchekorders/${orderId}/ssoURL`;

    const setOrderStatusMetadata = {
      ...metadata,
      url,
    };

    if (logger) logger.info("Retrieving iFrame url from accountcheck", setOrderStatusMetadata);

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Basic ${token}`,
      },
    };

    return axios.get(url, config).then((response: any) => {
      return new AccountChekIframeUrlResponse(response.data);
    });
  }

  getLiteData(
    token: string,
    baseUrl: string,
    orderId: string,
    logger: Logger,
    metadata: any
  ): Promise<Array<any>> {
    const url = `${baseUrl}/v1/accountchekorders/${orderId}/voa/lite`;

    const getLiteDataMetadata = {
      ...metadata,
      url,
    };

    if (logger) logger.info("Retrieving lite data from accountchek", getLiteDataMetadata);

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Basic ${token}`,
      },
    };

    return axios.get(url, config).then((response: any) => {
      return response.data;
    });
  }
}
