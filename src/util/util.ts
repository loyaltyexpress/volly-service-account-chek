/* eslint-disable no-useless-escape */
import { format } from "date-fns";

const getPreviousDayRange = (
  dateStr: string | null | undefined
): { start: string; end: string } => {
  if (!dateStr) dateStr = new Date().toString();

  const timestamp = Date.parse(dateStr);

  let date;
  if (Number.isNaN(timestamp)) {
    date = new Date();
  } else date = new Date(timestamp);

  const start = new Date(date);
  start.setMilliseconds(0);
  start.setSeconds(0);
  start.setMinutes(0);
  start.setHours(0);

  const end = new Date(date);
  end.setMilliseconds(59);
  end.setSeconds(59);
  end.setMinutes(59);
  end.setHours(23);

  return {
    start: format(start, "yyyy-MM-dd HH:mm:ss.sss xx"),
    end: format(end, "yyyy-MM-dd HH:mm:ss.sss xx"),
  };
};

const getBrowserType = (userAgent: string | null | undefined): "Mobile" | "Desktop" => {
  if (!userAgent) return "Desktop";

  const toMatch = [
    /Android/i,
    /webOS/i,
    /iPhone/i,
    /iPad/i,
    /iPod/i,
    /BlackBerry/i,
    /Windows Phone/i,
    /Opera Mini/i,
    /Mobile Safari/i,
  ];

  const matches = toMatch.some((toMatchItem) => {
    return userAgent.match(toMatchItem);
  });

  if (matches) return "Mobile";
  return "Desktop";
};

const resolveState = (stateAbbreviation?: string | null): string => {
  if (!stateAbbreviation) return "NA";
  switch (stateAbbreviation) {
    case "NA":
      return "NA";
    case "AA":
      return "Armed Forces Americas";
    case "AK":
      return "Alaska";
    case "AL":
      return "Alabama";
    case "AP":
      return "Armed Forces Pacific";
    case "AR":
      return "Arkansas";
    case "AZ":
      return "Arizona";
    case "CA":
      return "California";
    case "CO":
      return "Colorado";
    case "CT":
      return "Connecticut";
    case "DC":
      return "District of Columbia";
    case "DE":
      return "Delaware";
    case "FL":
      return "Florida";
    case "FM":
      return "Federated States of Micronesia";
    case "GA":
      return "Georgia";
    case "HI":
      return "Hawaii";
    case "IA":
      return "Iowa";
    case "ID":
      return "Idaho";
    case "IL":
      return "Illinois";
    case "IN":
      return "Indiana";
    case "KS":
      return "Kansas";
    case "KY":
      return "Kentucky";
    case "LA":
      return "Louisiana";
    case "MA":
      return "Massachusetts";
    case "MD":
      return "Maryland";
    case "ME":
      return "Maine";
    case "MH":
      return "Marshall Islands";
    case "MI":
      return "Michigan";
    case "MN":
      return "Minnesota";
    case "MO":
      return "Missouri";
    case "MP":
      return "Northern Mariana Islands";
    case "MS":
      return "Mississippi";
    case "MT":
      return "Montana";
    case "NC":
      return "North Carolina";
    case "ND":
      return "North Dakota";
    case "NE":
      return "Nebraska";
    case "NH":
      return "New Hampshire";
    case "NJ":
      return "New Jersey";
    case "NM":
      return "New Mexico";
    case "NV":
      return "Nevada";
    case "NY":
      return "New York";
    case "OH":
      return "Ohio";
    case "OK":
      return "Oklahoma";
    case "OR":
      return "Oregon";
    case "PA":
      return "Pennsylvania";
    case "PW":
      return "Palau";
    case "RI":
      return "Rhode Island";
    case "SC":
      return "South Carolina";
    case "SD":
      return "South Dakota";
    case "TN":
      return "Tennessee";
    case "TX":
      return "Texas";
    case "UT":
      return "Utah";
    case "VT":
      return "Virginia";
    case "WA":
      return "Washington";
    case "WI":
      return "Wisconsin";
    case "WV":
      return "West Virginia";
    case "WY":
      return "Wyoming";
    default:
      return "NA";
  }
};

const formatDate = (date?: string | Date | null, dateFormat?: string): string => {
  let formattedDate = "NA";
  if (!dateFormat) dateFormat = "yyyy-MM-dd";

  if (date) {
    try {
      formattedDate = format(new Date(date), dateFormat);
    } catch {
      // TODO - log this?
    }
  }
  return formattedDate;
};

const formatPhone = (phone?: string | null): number | null => {
  let formattedPhone: number | null = null;
  if (phone) {
    try {
      const phoneWithoutDashes = phone.replace(/-/g, "");
      formattedPhone = parseInt(phoneWithoutDashes, 10);
    } catch {
      // TODO log?
    }
  }
  return formattedPhone;
};

const pad = (val: string | number | null | undefined, width: number, padChar = "0"): string => {
  const str: string = val ? val.toString() : "";
  const diff = width - str.length;
  if (diff <= 0) return str;

  const padStr = new Array(diff).fill(padChar).join("");
  return `${padStr}${str}`;
};

export { formatDate, formatPhone, resolveState, pad, getBrowserType, getPreviousDayRange };
