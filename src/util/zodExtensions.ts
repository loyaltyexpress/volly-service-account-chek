import { z } from "zod";

/**
 * zod doesn't have a built-in json validator. this schema will ensure data
 * marked as JsonSchema will be a JSON object.
 *
 * Reference: https://github.com/colinhacks/zod#json-type
 */

type Literal = boolean | null | number | string;
type Json = Literal | { [key: string]: Json } | Json[];

const LiteralSchema = z.union([z.string(), z.number(), z.boolean(), z.null()]);
const JsonSchema: z.ZodSchema<Json> = z.lazy(() =>
  z.union([LiteralSchema, z.array(JsonSchema), z.record(JsonSchema)])
);

export { JsonSchema, Json };
